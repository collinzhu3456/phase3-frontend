import React, { useState } from "react";


import MediaGrid from "../Components/MedieGrid";

export const HomePage  = () => {

  return (
    <div className="App">
      <header className="App-header">
        <h1>This is the home page</h1>
        <MediaGrid/>

      </header>
    </div>
  );
};
