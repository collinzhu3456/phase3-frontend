import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

interface IMediaCardProps {
    Poster: string | undefined;
    Title: string | undefined;
    Year:string | undefined;

}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
     [theme.breakpoints.up('xl')]: {
        width:300,
        height:400,
      },
      paddingLeft:20,
      height:320,
      width:220
    },
    cardImage:{
      [theme.breakpoints.up('xl')]: {
        width:250,
        height:350,
      },
      marginRight:25,
      width:180,
      height:250

    },
    cardContent:{
        paddingTop: 0,
        paddingLeft:0
    },
    movieTitle:{
        fontSize:18,
        fontFamily:"Arial, Helvetica, sans-serif",
        marginTop:0,
        marginBottom:0,
        fontWeight:550
    },

    menuButton: {
      marginRight: theme.spacing(2),
      color:"white"
    },
    title: {
      flexGrow: 1,
      color:"white",
      position:"relative",
      left:"40%",
      fontSize:20,
      [theme.breakpoints.down('sm')]: {
        left:"20%",
        fontSize:15
      },
     
    },
  })
);

function MediaCard(props: IMediaCardProps) {
    const classes = useStyles();
    return (
        
            <Card className={classes.card}>
                <CardActionArea>
                    <CardMedia className = {classes.cardImage}
                        component="img"
                        image={props.Poster}
                    />
                    <CardContent className = {classes.cardContent}>
                        <p className = {classes.movieTitle}>{props.Title}</p>
                        <Typography variant="body2" color="textSecondary" component="p" className="MediaCardDescription">
                            {props.Year}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
    
    )
}

export default MediaCard