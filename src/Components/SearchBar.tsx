import React, { useState } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { IUserInput } from '../Common/Interfaces'


//Read the URL from the userinput
//call the post method in the handleSubmit funtion
//add a movie in the data array
function SearchBar() {
    return <div className="SearchBarContainer">
    <Grid container spacing={3}>
        <Grid item xs={6} sm={3}>
            <TextField
                required
                id="outlined-required"
                label="Search"
                variant="outlined"
 
   
            />
        </Grid>
        <Grid item xs={6} sm={3}>
            <Button variant="contained" color="primary" >
                Submit
            </Button>
        </Grid>
    </Grid>
</div>



}

export default SearchBar