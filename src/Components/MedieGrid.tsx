import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import MediaCard from './MediaCard'



interface IState{
    title:any;
    year:any;
    poster:any;
}

function MediaGrid() {
    const [ItemArray, setItemArray] = useState<IState[]>([{title: "",year: "",poster:""}])
    const updateMovie = () =>{
        fetch('https://mymoviesapi.azurewebsites.net/api/Movies')
        .then(response => response.json())
        .then(res => {
            setItemArray(res)
        })
        .catch(() => console.log("it didn't work")
        );

    }
    useEffect(() => {
        updateMovie();
    },[]);


    var Cards: JSX.Element[] = [];
    ItemArray?.forEach((el: IState, i: Number) => {
        if (!el || !el.title || !el.poster||!el.year) {
            return;
        }
        Cards.push(
            <Grid key={"card_"+i} item xs ={10} sm={6} md={2} className="MediaGridCard">
                <MediaCard Title = {el.title} Year = {el.year} Poster = {el.poster} />
            </Grid>)
    })
    return (
        <div>
            <h1></h1>
            <Grid container spacing={3} className="MediaGridContainer">
                {Cards}
            </Grid>
        </div>
    )
}

export default MediaGrid