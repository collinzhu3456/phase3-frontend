import React from "react";

import { createStyles, Grid, makeStyles, Theme } from "@material-ui/core";
import { SocialIcon } from "./SocialIcon";
import microsoftLogo from "../microsoft_logo.svg"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position:"static",
      bottom: 0,
      width: "100%",
      height: 45,
      textAlign: "center",
      fontSize: "12px",
      background: "black",
      color: "white",
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

const MICROSOFT_LOGO = {
    name: "Microsoft logo",
    url: "https://www.microsoft.com/en-nz",
    logo: microsoftLogo,
};

export default function Footer(){
    const classes = useStyles();
  
    return (
      <footer className={classes.root}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="flex-start"
          spacing={4}
        >
          <Grid item xs={4}>
            <SocialIcon {...MICROSOFT_LOGO} />
            {`Copyright © Microsoft Student Accelerator, 2022. All rights reserved`}
          </Grid>
        </Grid>
      </footer>
    );
  };