import React,{useState} from 'react';
import { IUserInput } from './Common/Interfaces';
import MediaGrid from './Components/MedieGrid';
import Header from './Components/Header';
import Footer from './Components/Footer'
import { Routes ,Route } from 'react-router-dom';
import {HomePage} from './Pages/HomePage'
import {SubmitPage} from './Pages/SubmitPage'



function App() {

 
  return (
    <div className="App">
        <Header></Header>
        <Routes>
          <Route path="/home" element={<HomePage/>} />
          <Route path="/submit" element={<SubmitPage/>}/>
        </Routes>
        {/* <MediaGrid /> */}
        <Footer></Footer>
    </div>
  );
}

export default App;
